<?php

/*
 * CommerceML PHP Library
 *
 * (c) 2016 Alexei Shabalin <mail@alshabalin.com>
 *
 * For the full copyright and license information, 
 * please read the LICENSE file that is distributed with this source code.
 */

namespace Lamotivo\CommerceML;

use JsonSerializable;
use SimpleXMLElement;

/**
 * @author Alexei Shabalin <mail@alshabalin.com>
 */
class CommerceML implements JsonSerializable
{

    /**
     * @var Lamotivo\CommerceML\Entity\Catalog $catalog
     */
    public $catalog;

    /**
     * @var Lamotivo\CommerceML\Entity\PriceList $price_list
     */
    public $price_list;

    /**
     * @param mixed $import_xml
     * @param mixed $offers_xml
     * @return Lamotivo\CommerceML\CommerceML
     */
    public function __construct($import_xml = null, $offers_xml = null)
    {
        if ($import_xml !== null)
        {
            $this->load_import($import_xml);
        }

        if ($offers_xml !== null)
        {
            $this->load_offers($offers_xml);
        }
    }

    /**
     * @param mixed $import_xml
     * @param mixed $offers_xml
     * @return Lamotivo\CommerceML\CommerceML
     */
    public static function import($import_xml, $offers_xml = null)
    {
        return new static($import_xml, $offers_xml);
    }

    /**
     * @param mixed $import_xml
     * @return void
     */
    public function load_import($import_xml = null)
    {
        if ($import_xml !== null)
        {
            $this->catalog = new Entity\Catalog($this->load($import_xml));
        }
    }

    /**
     * @param mixed $offers_xml
     * @return void
     */
    public function load_offers($offers_xml = null)
    {
        if ($offers_xml !== null)
        {
            $this->price_list = new Entity\PriceList($this->load($offers_xml));
        }
    }

    /**
     * @param mixed $xml
     * @return SimpleXMLElement
     */
    protected function load($xml)
    {
        if (is_string($xml) && is_file($xml))
        {
            return simplexml_load_file($xml);
        }

        if ( ! is_a($xml, SimpleXMLElement::class))
        {
            return simplexml_load_string($xml);
        }

        return $xml;
    }


    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'catalog' => $this->catalog,
            'price_list' => $this->price_list,
        ];
    }
}
