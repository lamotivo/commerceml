<?php

namespace Lamotivo\CommerceML\Entity;

class PropertyValueCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = PropertyValue::class;
}
