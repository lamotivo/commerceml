<?php

namespace Lamotivo\CommerceML\Entity;

class PriceTypeCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = PriceType::class;
}
