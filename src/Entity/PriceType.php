<?php

namespace Lamotivo\CommerceML\Entity;

class PriceType extends AbstractEntity
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Ид' => 'uuid',
        'Наименование' => 'type',
        'Валюта' => 'currency',
    ];

    /**
     * @var string
     */
    public $uuid;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $currency;

}
