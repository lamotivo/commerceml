<?php

namespace Lamotivo\CommerceML\Entity;

class StockCountCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = StockCount::class;
}
