<?php

namespace Lamotivo\CommerceML\Entity;

use SimpleXMLElement;

abstract class AbstractEntity
{
    /**
     * @var array
     */
    protected static $mapping = [];

    /**
     * @var array
     */
    protected static $object_mapping = [];

    /**
     * @var array
     */
    // protected $unknown_properties = [];

    /**
     * @param SimpleXMLElement|null $xml
     * @return Lamotivo\CommerceML\Entity\AbstractEntity
     */
    public function __construct($xml = null)
    {
        if ($xml !== null && is_a($xml, SimpleXMLElement::class))
        {
            foreach (static::$mapping as $origin => $attribute)
            {
                if ($xml->xpath($origin))
                {
                    $value = $xml->xpath($origin);
                }
                else
                {
                    continue;
                }

                if (is_array($attribute))
                {
                    list($attribute, $class_name) = $attribute;

                    if ( ! is_subclass_of($class_name, AbstractCollection::class))
                    {
                        $value = new $class_name($value[0]);
                    }
                    else
                    {
                        $value = new $class_name($value);
                    }
                }
                else
                {
                    $value = (string)$value[0];
                }

                if (is_string($attribute))
                {
                    $this->$attribute = $value;
                }
                else
                {
                    // $this->unknown_properties[$attribute] = $value;
                }
            }
        }
    }

}
