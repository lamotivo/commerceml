<?php

namespace Lamotivo\CommerceML\Entity;

class StockCount extends StringValue
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        '@ИдСклада' => 'uuid',
        '@КоличествоНаСкладе' => 'count',
    ];

    /**
     * {@inheritdoc}
     */
    public $uuid;

    /**
     * {@inheritdoc}
     */
    public $count;


    /**
     * {@inheritdoc}
     */
    public function toId()
    {
        return $this->uuid;
    }

    /**
     * {@inheritdoc}
     */
    public function toString()
    {
        return $this->count;
    }

}
