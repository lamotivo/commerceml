<?php

namespace Lamotivo\CommerceML\Entity;

class TaxCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = Tax::class;
}
