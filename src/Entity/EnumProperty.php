<?php

namespace Lamotivo\CommerceML\Entity;

class EnumProperty extends Property
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Ид' => 'uuid',
        'Наименование' => 'name',
        'ВариантыЗначений/Справочник' => [
            'enum',
            EnumCollection::class
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public $uuid;

    /**
     * {@inheritdoc}
     */
    public $name;

    /**
     * @var Lamotivo\CommerceML\Entity\EnumCollection $enum
     */
    public $enum;

}
