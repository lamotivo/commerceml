<?php

namespace Lamotivo\CommerceML\Entity;

class Enum extends StringValue
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'ИдЗначения' => 'uuid',
        'Значение' => 'name',
    ];
}
