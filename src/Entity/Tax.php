<?php

namespace Lamotivo\CommerceML\Entity;

class Tax extends StringValue
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Наименование' => 'uuid',
        'Ставка' => 'name',
    ];
}
