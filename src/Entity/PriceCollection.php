<?php

namespace Lamotivo\CommerceML\Entity;

class PriceCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = Price::class;
}
