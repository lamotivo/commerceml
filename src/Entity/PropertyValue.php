<?php

namespace Lamotivo\CommerceML\Entity;

class PropertyValue extends StringValue
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Ид' => 'uuid',
        'Значение' => 'name',
    ];
}
