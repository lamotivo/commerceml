<?php

namespace Lamotivo\CommerceML\Entity;

class Attribute extends StringValue
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Наименование' => 'uuid',
        'Значение' => 'name',
    ];
}
