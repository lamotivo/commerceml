<?php

namespace Lamotivo\CommerceML\Entity;

class StringValueCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = StringValue::class;
}
