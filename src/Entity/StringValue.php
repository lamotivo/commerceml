<?php

namespace Lamotivo\CommerceML\Entity;

class StringValue extends AbstractEntity
{
    /**
     * @var string $uuid
     */
    public $uuid;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string
     */
    public function toId()
    {
        return $this->uuid;
    }

    /**
     * @var string
     */
    public function toString()
    {
        return $this->name;
    }
}
