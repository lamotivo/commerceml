<?php

namespace Lamotivo\CommerceML\Entity;

class Price extends StringValue
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'ИдТипаЦены' => 'uuid',
        'ЦенаЗаЕдиницу' => 'name',
        'Валюта' => 'currency',
        'Единица' => 'unit',
        'Коэффициент' => 'coeff',
    ];

    /**
     * @var string $uuid
     */
    public $uuid;

    /**
     * @var float|integer $name
     */
    public $name;

    /**
     * @var string $currency
     */
    public $currency;

    /**
     * @var string $unit
     */
    public $unit;

    /**
     * @var float $coeff
     */
    public $coeff;

}
