<?php

namespace Lamotivo\CommerceML\Entity;

class CategoryCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = Category::class;
}
