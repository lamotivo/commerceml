<?php

namespace Lamotivo\CommerceML\Entity;

class EnumCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = Enum::class;
}
