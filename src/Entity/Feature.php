<?php

namespace Lamotivo\CommerceML\Entity;

class Feature extends StringValue
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Ид' => 'uuid',
        'Наименование' => 'name',
        'Значение' => 'value',
    ];

    /**
     * {@inheritdoc}
     */
    public $uuid;

    /**
     * {@inheritdoc}
     */
    public $name;

    /**
     * @var string $value
     */
    public $value;

    /**
     * {@inheritdoc}
     */
    public function toId()
    {
        return $this->uuid ?: $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function toString()
    {
        return $this->value ?: $this->name;
    }

}
