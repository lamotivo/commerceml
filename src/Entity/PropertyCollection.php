<?php

namespace Lamotivo\CommerceML\Entity;

class PropertyCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = Property::class;

    protected function resolve($element)
    {
        if ((string)$element->ТипЗначений === 'Справочник')
        {
            return new EnumProperty($element);
        }

        return new Property($element);
    }
}
