<?php

namespace Lamotivo\CommerceML\Entity;

class Property extends AbstractEntity
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Ид' => 'uuid',
        'Наименование' => 'name',
    ];

    /**
     * @var string $uuid
     */
    public $uuid;

    /**
     * @var string $name
     */
    public $name;

}
