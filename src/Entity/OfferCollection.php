<?php

namespace Lamotivo\CommerceML\Entity;

class OfferCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = Offer::class;
}
