<?php

namespace Lamotivo\CommerceML\Entity;

class AttributeCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = Attribute::class;
}
