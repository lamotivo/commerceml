<?php

namespace Lamotivo\CommerceML\Entity;

class FeatureCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = Feature::class;
}
