<?php

namespace Lamotivo\CommerceML\Entity;

class Catalog extends AbstractEntity
{

    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        '@ДатаФормирования' => [
            'created_at',
            DateTime::class
        ],
        'Каталог/Ид' => 'uuid',
        'Каталог/Наименование' => 'name',

        'Классификатор/Владелец' => [
            'owner',
            Owner::class
        ],
        'Классификатор/Группы/Группа' => [
            'categories',
            CategoryCollection::class
        ],
        'Классификатор/Свойства/Свойство' => [
            'properties',
            PropertyCollection::class
        ],

        'Каталог/Товары/Товар' => [
            'products',
            ProductCollection::class
        ],
    ];


    /**
     * @var string $uuid
     */
    public $uuid;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var DateTime $created_at
     */
    public $created_at;

    /**
     * @var Lamotivo\CommerceML\Entity\Owner $owner
     */
    public $owner;

    /**
     * @var Lamotivo\CommerceML\Entity\CategoryCollection $categories
     */
    public $categories;

    /**
     * @var Lamotivo\CommerceML\Entity\PropertyCollection $properties
     */
    public $properties;

    /**
     * @var Lamotivo\CommerceML\Entity\ProductCollection $products
     */
    public $products;

}
