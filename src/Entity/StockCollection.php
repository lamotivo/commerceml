<?php

namespace Lamotivo\CommerceML\Entity;

class StockCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = Stock::class;
}
