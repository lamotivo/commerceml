<?php

namespace Lamotivo\CommerceML\Entity;

use ArrayAccess;
use IteratorAggregate;
use SimpleXMLElement;
use JsonSerializable;
use ArrayIterator;

abstract class AbstractCollection implements ArrayAccess, IteratorAggregate, JsonSerializable
{
    /**
     * @var string
     */
    protected static $element_class_name;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param array|null $xml
     * @return Lamotivo\CommerceML\Entity\AbstractCollection
     */
    public function __construct(array $xml = null)
    {
        if ($xml !== null)
        {
            foreach ($xml as $element)
            {
                $this->add($element);
            }
        }
    }

    /**
     * @param Lamotivo\CommerceML\Entity\AbstractEntity|mixed $element
     * @return Lamotivo\CommerceML\Entity\AbstractEntity|string
     */
    protected function resolve($element)
    {
        if ($element_class_name = static::$element_class_name)
        {
            return new $element_class_name($element);
        }
        else
        {
            return trim((string)$element);
        }
    }

    /**
     * @param Lamotivo\CommerceML\Entity\AbstractEntity|mixed $element
     */
    protected function add($element)
    {
        $element = $this->resolve($element);

        if (is_string($element))
        {
            $this->data[] = trim($element);
        }
        else
        {
            if (is_a($element, StringValue::class))
            {
                $this->data[$element->toId()] = trim($element->toString());
            }
            else
            {
                $this->data[$element->uuid] = $element;
            }
        }
    }


    /**
     * @param string|int $offset
     * @return bool
     */
    public function offsetExists($offset)
    {          
        return isset($this->data[$offset]);
    }    

    /**
     * @param string|int $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {   
        return $this->offsetExists($offset) ? $this->data[$offset] : null;
    }

    /**
     * @param string|int $offset
     * @param Lamotivo\CommerceML\Entity\AbstractEntity $entity
     * @return void
     */
    public function offsetSet($offset, $entity)
    {          
        if ($offset)
        {
            $this->data[$offset] = $entity;
        }
        else
        {
            $this->data[] = $entity;
        }
    }

    /**
     * @param string|int $offset
     * @return mixed
     */
    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset))
        {
            unset($this->data[$offset]);
        }
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->data);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->data;
    }
}
