<?php

namespace Lamotivo\CommerceML\Entity;

class Stock extends AbstractEntity
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Ид' => 'uuid',
        'Наименование' => 'name',
    ];

    /**
     * {@inheritdoc}
     */
    public $uuid;

    /**
     * {@inheritdoc}
     */
    public $name;

}
