<?php

namespace Lamotivo\CommerceML\Entity;

class ProductCollection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected static $element_class_name = Product::class;
}
