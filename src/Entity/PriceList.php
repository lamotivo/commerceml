<?php

namespace Lamotivo\CommerceML\Entity;

class PriceList extends AbstractEntity
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        '@ДатаФормирования' => [
            'created_at',
            DateTime::class
        ],

        'ПакетПредложений/Ид' => 'uuid',
        'ПакетПредложений/Наименование' => 'name',
        'ПакетПредложений/ИдКаталога' => 'catalog_uuid',
        'ПакетПредложений/ИдКлассификатора' => 'classificator_uuid',

        'ПакетПредложений/Владелец' => [
            'owner',
            Owner::class
        ],
        'ПакетПредложений/ТипыЦен/ТипЦены' => [
            'price_types',
            PriceTypeCollection::class
        ],
        'ПакетПредложений/Склады/Склад' => [
            'stocks',
            StockCollection::class
        ],
        'ПакетПредложений/Предложения/Предложение' => [
            'offers',
            OfferCollection::class
        ],
    ];


    /**
     * @var string $uuid
     */
    public $uuid;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $catalog_uuid
     */
    public $catalog_uuid;

    /**
     * @var string $classificator_uuid
     */
    public $classificator_uuid;

    /**
     * @var DateTime $created_at
     */
    public $created_at;

    /**
     * @var string $owner
     */
    public $owner;

    /**
     * @var Lamotivo\CommerceML\Entity\PriceTypeCollection $price_types
     */
    public $price_types;

    /**
     * @var Lamotivo\CommerceML\Entity\StockCollection $stocks
     */
    public $stocks;

    /**
     * @var Lamotivo\CommerceML\Entity\OfferCollection $offers
     */
    public $offers;


    public function get_prices($type)
    {
        if ( ! isset($this->price_types[$type]))
        {
            foreach ($this->price_types as $id => $price_type)
            {
                if ($price_type->type === $type)
                {
                    return $this->get_prices($id);
                }
            }

            return [];
        }

        $prices = [];

        foreach ($this->offers as $id => $offer)
        {
            $prices[$id] = (object)[
                'unit' => $offer->unit,
                'count' => $offer->count,
                'cost' => (float)$offer->prices[$type]->cost,
                'currency' => $offer->prices[$type]->currency,
            ];
        }

        return $prices;
    }

}
