<?php

namespace Lamotivo\CommerceML\Entity;

class Bank extends AbstractEntity
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Банк/БИК' => 'bik',
        'Банк/СчетКорреспондентский' => 'correspondent_account',
        'Банк/Наименование' => 'name',
        'НомерСчета' => 'account',
    ];


    /**
     * @var string $id
     */
    public $bik;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $correspondent_account
     */
    public $correspondent_account;

    /**
     * @var string $account
     */
    public $account;
}
