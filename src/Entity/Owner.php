<?php

namespace Lamotivo\CommerceML\Entity;

class Owner extends AbstractEntity
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Ид' => 'uuid',
        'Наименование' => 'name',
        'ОфициальноеНаименование' => 'legal_name',
        'ЮридическийАдрес/Представление' => 'address',
        'ИНН' => 'inn',
        'КПП' => 'kpp',
        'ОКПО' => 'okpo',
        'РасчетныеСчета/РасчетныйСчет' => ['bank', Bank::class],
    ];


    /**
     * @var string $uuid
     */
    public $uuid;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $legal_name
     */
    public $legal_name;

    /**
     * @var string $address
     */
    public $address;

    /**
     * @var string $inn
     */
    public $inn;

    /**
     * @var string $kpp
     */
    public $kpp;

    /**
     * @var string $okpo
     */
    public $okpo;

    /**
     * @var Lamotivo\CommerceML\PropertyCollection\Bank $bank
     */
    public $bank;
}
