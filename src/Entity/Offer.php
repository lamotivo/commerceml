<?php

namespace Lamotivo\CommerceML\Entity;

class Offer extends AbstractEntity
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Ид' => 'uuid',
        'Артикул' => 'plu',
        'Наименование' => 'name',
        'Количество' => 'count',
        'Склад' => [
            'stock_counts',
            StockCountCollection::class
        ],
        'БазоваяЕдиница' => 'unit',
        'Цены/Цена' => [
            'prices',
            PriceCollection::class
        ],
        'ХарактеристикиТовара/ХарактеристикаТовара' => [
            'features',
            FeatureCollection::class
        ],
    ];

    /**
     * @var string $uuid
     */
    public $uuid;

    /**
     * @var string $plu
     */
    public $plu;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var integer $count
     */
    public $count;

    /**
     * @var Lamotivo\CommerceML\StockCountCollection $store_counts
     */
    public $stock_counts;

    /**
     * @var string $unit
     */
    public $unit;

    /**
     * @var Lamotivo\CommerceML\PriceCollection $prices
     */
    public $prices;

    /**
     * @var Lamotivo\CommerceML\FeatureCollection $features
     */
    public $features;

}
