<?php

namespace Lamotivo\CommerceML\Entity;

class Product extends AbstractEntity
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Ид' => 'uuid',
        'Артикул' => 'plu',
        'Штрихкод' => 'barcode',
        'БазоваяЕдиница' => 'unit',
        'Наименование' => 'name',
        'Описание' => 'description',
        'Картинка' => 'image',
        'Группы/Ид' => [
            'categories',
            ValueList::class
        ],
        'ЗначенияРеквизитов/ЗначениеРеквизита' => [
            'attributes',
            AttributeCollection::class
        ],
        'ЗначенияСвойств/ЗначенияСвойства' => [
            'properties',
            PropertyValueCollection::class
        ],
        'ХарактеристикиТовара/ХарактеристикаТовара' => [
            'features',
            FeatureCollection::class
        ],
        'СтавкиНалогов/СтавкаНалога' => [
            'taxes',
            TaxCollection::class
        ],
    ];

    /**
     * @var string $uuid
     */
    public $uuid;

    /**
     * @var string $plu
     */
    public $plu;

    /**
     * @var string $barcode
     */
    public $barcode;

    /**
     * @var string $unit
     */
    public $unit;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var string $image
     */
    public $image;

    /**
     * @var Lamotivo\CommerceML\PropertyCollection $properties
     */
    public $properties;

    /**
     * @var Lamotivo\CommerceML\FeatureCollection $features
     */
    public $features;

    /**
     * @var Lamotivo\CommerceML\AttributeCollection $attributes
     */
    public $attributes;

    /**
     * @var Lamotivo\CommerceML\TaxCollection $taxes
     */
    public $taxes;

    /**
     * @var array $categories
     */
    public $categories;


}
