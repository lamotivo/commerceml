<?php

namespace Lamotivo\CommerceML\Entity;

class Category extends AbstractEntity
{
    /**
     * {@inheritdoc}
     */
    protected static $mapping = [
        'Ид' => 'uuid',
        'Наименование' => 'name',
        'Группы/Группа' => [
            'categories',
            CategoryCollection::class
        ],
    ];

    /**
     * @var string $uuid
     */
    public $uuid;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var Lamotivo\CommerceML\Entity\CategoryCollection $categories
     */
    public $categories;

}
