# CommerceML PHP Library

Читает CommerceML-файл, разбирает на сущности.

Пример использования:

```php

use Alshabalin\CommerceML\CommerceML;

$import_xml = 'import.xml';

$offers_xml = 'offers.xml';

$cml = CommerceML::import($import_xml, $offers_xml);

// foreach ($cml->catalog->products as $product) { .. }

// foreach ($cml->price_list->offers as $offer) { .. }

```
